devtools::load_all()

options(shiny.maxRequestSize = 1e3 * 1024 ^ 2,
        DT.options = list(scrollY = "60vh"))

shinyApp(
  ui = bslib::page_navbar(title = 'Knowledge Graphs',
                        # tags$head(tags$style(HTML("
                        #       .selectize-control .selectize-input {
                        #         max-height: 300px;
                        #         overflow-y: auto;
                        #       }
                        #                           ")))
                          kgraphUI('kgraph_embeds', type = 'embeds'),
                          kgraphUI('kgraph_cooc', type = 'cooc'),
                          kgraphUI('kgraph_relations', type = 'relations')),

  server = function(input, output, session) {
      kgraphServer('kgraph_embeds', type = 'embeds')
      kgraphServer('kgraph_cooc', type = 'cooc')
      kgraphServer('kgraph_relations', type = 'relations')
  })

