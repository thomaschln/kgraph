# prepare the package for release
PKGNAME := $(shell sed -n "s/Package: *\([^ ]*\)/\1/p" DESCRIPTION)
PKGVERS := $(shell sed -n "s/Version: *\([^ ]*\)/\1/p" DESCRIPTION)
PKGSRC  := $(shell basename `pwd`)

all: clean devtools_check

doc.pdf:
	R CMD Rd2pdf -o doc.pdf .

build:
	cd ..;\
	R CMD build --no-manual $(PKGSRC)

build-cran:
	cd ..;\
	R CMD build $(PKGSRC)

install: build
	cd ..;\
	R CMD INSTALL $(PKGNAME)_$(PKGVERS).tar.gz

check: build-cran
	cd ..;\
	R CMD check $(PKGNAME)_$(PKGVERS).tar.gz --as-cran

submit: check
	cd ..;\
	mv $(PKGNAME)_$(PKGVERS).tar.gz $(PKGSRC)

roxygenise:
	R -e "roxygen2::roxygenise()"

devtools_test:
	R -e "devtools::test()"

devtools_check:
	R -e "devtools::check()"

vignette:
	cd vignettes;\
	R -e "rmarkdown::render('kgraph.Rmd')"

clean:
	$(RM) doc.pdf

build_cuis_pairs:
	wget -O kg.csv https://dataverse.harvard.edu/api/access/datafile/6180620
	wget -O umls_mondo.csv "https://www.dropbox.com/scl/fi/bj9i6c6em1zsq3exixpid/umls_mondo.csv?rlkey=vwbpmgethp7wcssgxy4cyaye2&st=ts4j9p4t&dl=0"
	mv *.csv inst/extdata/
	R -e "source('scripts/primekg_to_cuis.R')"

build_dictionary:
	# need to download NILE and populate portable_NILE in inst, see vignette for URL
	wget -O m_embeds.csv "https://www.dropbox.com/scl/fi/v21zcpf59y6pk6te1hmoq/epmc_1700_glove_fit_nile.rds?rlkey=vxkstqcuk2hjpb8sy4syj9glh&dl=0"
	R -e "source('scripts/build_dictionary.R')"

subset_mh_embeds: build_dictionary build_cuis_pairs
	R -e "source('scripts/subset_mh_embeds.R')"

build_gwas_data:
	# need to download csv from https://www.ebi.ac.uk/gwas/efotraits/EFO_0007623
	# and add it to extdata folder
	R -e "source('scripts/gwas_data.R')"
