from rocker/shiny-verse:4.4.1
run apt-get update && \
  apt-get install -y --no-install-recommends texlive texlive-latex-recommended texlive-fonts-extra qpdf && \
  # for igraph
  apt-get install -y libxml2-dev libglpk-dev

run R -e "install.packages(c('dplyr', 'reshape2', 'RColorBrewer', 'pROC', 'text2vec', 'flexdashboard'))"

run apt-get update && apt-get install -y git
run R -e "remotes::install_git('https://gitlab.com/thomaschln/sgraph.git', ref = 'b147d4ca00983197d5839c9a26805eab4429ec4e')"

run apt-get update && apt-get install -y tidy

run R -e "install.packages('DT')"

add ./DESCRIPTION /kgraph/DESCRIPTION
run R -e "devtools::install_deps('kgraph', dependencies = TRUE)"

run R -e "install.packages('R.utils')"
run R -e "install.packages('rsvd')"

add ./ /kgraph
run R -e "devtools::install('kgraph', dependencies = TRUE)"
