---
title: "Knowledge Graphs"
output: 
  flexdashboard::flex_dashboard:
    orientation: rows
    theme: spacelab 
runtime: shiny
---

```{css selectize-input, echo = FALSE}
.selectize-control .selectize-input {
  max-height: 300px;
  overflow-y: auto;
}
```


```{r}
  # 1GB file limit
  options(shiny.maxRequestSize = 1e3 * 1024 ^ 2)
  devtools::load_all()
  #library(kgraph)
# dirpath = system.file('data', package = 'kgraph')
# m_embeds = get(load(file.path(dirpath, 'm_embeds.rds')))

# df_dict = get(load(file.path(dirpath, 'df_dict.rds')))
# df_dict$desc = df_dict$label
# df_dict$color = df_dict$group_details

# target_desc = paste(target_strs, collapse = '|') %>%
#     grep(df_dict$desc, value = TRUE) %>% head(3)

# if (length(target_desc) == 0) {
#     target_desc = sample(df_dict$desc, 3)
# }
  colors_mapping = NULL

  similarity = 'cosine'; threshold_projs = 5; label_grid_size = 150; use_known_pairs = 'None'
  set.seed(1)

  validate_input_file = function(df_input) {
    fpath = df_input$datapath
    switch(tools::file_ext(df_input$name),
           csv = data.table::fread(fpath, sep = ",", data.table = FALSE),
           tsv = data.table::fread(fpath, sep = "\t", data.table = FALSE),
           Rdata = get(load(fpath)),
           rds = get(load(fpath)),
           validate("Invalid file; Please upload a .csv, .tsv, .Rdata, or .rds file"))
  }


  react_embeds <- reactive({

      df_input = input$upload_embeds
      if (is.null(df_input)) return()

      m_embeds = validate_input_file(df_input)
  if (is.null(rownames(m_embeds))) rownames(m_embeds) = seq_len(nrow(m_embeds))
  m_embeds
    })

  react_weights = reactive({
      df_input = input$upload_weights
      if (is.null(df_input)) return()

      df_dict = validate_input_file(df_input)
    })

  react_dict = reactive({

      df_input = input$upload_dict
      if (is.null(df_input)) return()

      df_dict = validate_input_file(df_input)
    })

  react_init_concepts <- reactive({

      m_embeds = react_embeds()

      if (!is.null(m_embeds)) {

        sample(rownames(m_embeds), 2)

      } else {

        df_weights = react_weights()
        if (is.null(df_weights)) return()

        # these combinations share some SNPs
        # http://www.ebi.ac.uk/efo/EFO_0004320
        # http://www.ebi.ac.uk/efo/EFO_0007623
        # http://www.ebi.ac.uk/efo/EFO_0007624

        # http://www.ebi.ac.uk/efo/EFO_0004321
        # http://www.ebi.ac.uk/efo/EFO_0004247

        # error with 
        #[1] "http://www.ebi.ac.uk/efo/EFO_0000677"
        #[2] "http://www.ebi.ac.uk/efo/EFO_0004321"
        ### single groups shouldnt appear
        ### colors error


        sample(unique(df_weights$concept1), 2)
      }
    })


  react_pairs = shiny::reactive({
      
      use_known_pairs = input$use_known_pairs

# df_pairs = get(load(file.path(dirpath, 'df_cuis_pairs.rds')))
# df_pairs_cols = c('umls_id.x', 'umls_id.y')

      df_pairs_fit = if (use_known_pairs == 'CUIs') df_pairs[df_pairs_cols] else NULL 
  })

  react_fit = shiny::reactive({

      similarity = input$similarity
      threshold_projs = input$threshold_projs
      m_embeds = react_embeds()
      df_pairs_fit = react_pairs()

      if (is.null(m_embeds)) return()

      fit_kg = fit_embeds_kg(m_embeds, df_pairs = df_pairs_fit,
						   similarity = similarity,
						   threshold_projs = 1 - threshold_projs / 100)
    })


  react_kgraph = shiny::reactive({

      m_embeds = react_embeds()
      df_dict = react_dict()
      fit_kg = react_fit()
      target_concepts = input$selected_concepts
      df_weights = react_weights()

      if (!is.null(m_embeds)) {

        kgraph = build_kgraph_from_fit(target_concepts, m_embeds, fit_kg,
                                       df_dict = df_dict)
      } else {

        kgraph = build_kgraph(target_concepts, df_weights, df_dict = df_dict)
      }
    })

  react_colors = shiny::reactive({

      df_dict = react_dict()
      if (is.null(df_dict)) return()

      colors_mapping = if ('color' %in% names(df_dict)) {
        get_color_map(c(unique(df_dict$color), 'Groups', 'Other'))
      }
  })

  react_sgraph = shiny::reactive({

      kgraph = react_kgraph()
      label_grid_size = input$label_grid_size
      colors_mapping = react_colors()

      sgraph = get_sgraph(kgraph, colors_mapping,
			  label_grid_cell_size = label_grid_size)
    })

  react_legend = shiny::reactive({

      kgraph = react_kgraph()
      colors_mapping = react_colors()

      if (is.null(colors_mapping)) return()

      gglegend = sgraph:::get_legend(colors_mapping,
					 unique(kgraph$df_nodes$clusters))
    })

  react_auc = shiny::reactive({

      fit_kg = react_fit()

      pROC::plot.roc(fit_kg$roc, print.auc = TRUE)
    })

  output$targets = shiny::renderUI({
      m_embeds = react_embeds()
      targets = react_init_concepts()
      df_weights = react_weights()

      choices = if (!is.null(m_embeds)) {
        rownames(m_embeds)
      } else {
        df_weights$concept1
      }

      shiny::selectInput('selected_concepts', 'Selected Concepts',
                         sort(unique(choices)), multiple = TRUE,
                         selected = targets, selectize = TRUE)
    })

  output$kg = sgraph::renderSgraph(react_sgraph())
  output$legend = shiny::renderPlot(grid::grid.draw(react_legend()))
  output$auc = shiny::renderPlot(react_auc(), res = 96)

```

# Options {.sidebar}

##

```{r}

  shiny::fileInput('upload_embeds', 'Embedding matrix',
                   accept = c('.csv', '.tsv', '.Rdata', '.rds'))

  shiny::selectInput('similarity', 'Similarity', 
    #c('inprod', 'cosine', 'cov_simi', 'norm_inprod'), selected = 'inprod')
    c('inprod', 'cosine'), selected = 'cosine')

  shiny::selectInput('use_known_pairs', 'Known pairs data frame', c('CUIs', 'None'), 'None')

  shiny::sliderInput('threshold_projs', 'False positive threshold (%)',
		     0, 50, 5)

  shiny::fileInput('upload_weights', 'Concepts relations data frame',
                   accept = c('.csv', '.tsv', '.Rdata', '.rds'))

  shiny::fileInput('upload_dict', 'Dictionary data frame upload',
                   accept = c('.csv', '.tsv', '.Rdata', '.rds'))

  shiny::uiOutput('targets')

  # probably could depend on window size
  shiny::sliderInput('label_grid_size',
                     htmltools::HTML('Label grid size</br>(Smaller = more labels)'),
                     1, 2e3, 150, ticks = FALSE)
```

# Graph

##

###

```{r}
  # some target nodes have no groups ?
  fillPage(sgraph::sgraphOutput('kg', height = '100%'),
           absolutePanel(id = 'legend', class = 'panel panel-default',
                         fixed = TRUE, draggable = TRUE, top = 100,
    	                 left = 'auto', right = 20, bottom = 'auto',
    	                 width = 120, height = '400px',
                         plotOutput('legend', height = '400px')))
```

# AUC

##

###

```{r}
  shiny::plotOutput('auc', height = '100%')
```
